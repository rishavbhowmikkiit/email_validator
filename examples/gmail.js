const lambdaLocal = require('lambda-local');
const assert = require('assert')

//valid gamil account
async function test1() {
    const jsonPayload = {
        body: { email: "b19kiit@gmail.com" }
    }
    const done = (await lambdaLocal.execute({
        event: jsonPayload,
        lambdaPath: './index.js',
        profileName: 'default',
        timeoutMs: 60_000 //one minute
    })).body
    console.log(`EMAIL: ${jsonPayload.body.email} : Testing results ..........................................`);
    assert(done.error === null,
        `EMAIL: ${jsonPayload.body.email}\n{error}(must be null): ${done.error}`
    )
    assert(done.valid === true,
        `EMAIL: ${jsonPayload.body.emai}\n{valid}(must be true): '${done.valid}'`
    )
    assert(done.port_25_blocked === false,
        `EMAIL: ${jsonPayload.body.email}\n{port_25_blocked}(must be true): ${done.port_25_blocked}\BUT GREAT NEWS IF IT'S FALSE`
    )
    assert(Array.isArray(done.dns_records) && (done.dns_records.length > 0),
        `EMAIL: ${jsonPayload.body.emai}\n{dns_records}(must be an Array with aleast one element): '${done.valid}'`
    )
    console.log( "SUCCESS", done); 
}

//invalid gmail account
async function test2() {
    const jsonPayload = {
        body: { email: "b1900009999kiit@gmail.com" }
    }
    const done = (await lambdaLocal.execute({
        event: jsonPayload,
        lambdaPath: './index.js',
        profileName: 'default',
        timeoutMs: 60_000 //one minute
    })).body
    console.log(`EMAIL: ${jsonPayload.body.email} : Testing results ..........................................`);
    assert(done.error === null,
        `EMAIL: ${jsonPayload.body.email}\n{error}(must be null): ${done.error}`
    )
    assert(done.valid === false,
        `EMAIL: ${jsonPayload.body.email}\n{valid}(must be false): '${done.valid}'`
    )
    assert(done.port_25_blocked === false,
        `EMAIL: ${jsonPayload.body.email}\n{port_25_blocked}(must be false): ${done.port_25_blocked}\BUT BAD NEWS IF IT'S FALSE (gmail smtp server have open port 25)`
    )
    assert(Array.isArray(done.dns_records) && (done.dns_records.length > 0),
        `EMAIL: ${jsonPayload.body.emai}\n{dns_records}(must be an Array with aleast one element): '${done.valid}'`
    )
    console.log( "SUCCESS", done);
}

//valid gsuit account
async function test3() {
    const jsonPayload = {
        body: { email: "1706349@kiit.ac.in" }
    }
    const done = (await lambdaLocal.execute({
        event: jsonPayload,
        lambdaPath: './index.js',
        profileName: 'default',
        timeoutMs: 60_000 //one minute
    })).body
    console.log(`EMAIL: ${jsonPayload.body.email} : Testing results ..........................................`);
    assert(done.error === null,
        `EMAIL: ${jsonPayload.body.email}\n{error}(must be null): ${done.error}`
    )
    assert(done.valid === true,
        `EMAIL: ${jsonPayload.body.email}\n{valid}(must be true): '${done.valid}'`
    )
    assert(done.port_25_blocked == false,
        `EMAIL: ${jsonPayload.body.email}\n{port_25_blocked}(must be false): ${done.port_25_blocked}\BUT GREAT NEWS IF IT'S FALSE`
    )
    assert(Array.isArray(done.dns_records) && (done.dns_records.length > 0),
        `EMAIL: ${jsonPayload.body.email}\n{dns_records}(must be an Array with aleast one element): '${done.valid}'`
    )
    console.log( "SUCCESS", done); 
}

//invalid gsuit account
async function test4() {
    const jsonPayload = {
        body: { email: "299706349@kiit.ac.in" }
    }
    const done = (await lambdaLocal.execute({
        event: jsonPayload,
        lambdaPath: './index.js',
        profileName: 'default',
        timeoutMs: 60_000 //one minute
    })).body
    console.log(`EMAIL: ${jsonPayload.body.email} : Testing results ..........................................`);
    assert(done.error === null,
        `EMAIL: ${jsonPayload.body.email}\n{error}(must be null): ${done.error}`
    )
    assert(done.valid === false,
        `EMAIL: ${jsonPayload.body.email}\n{valid}(must be false): '${done.valid}'`
    )
    assert(done.port_25_blocked === false,
        `EMAIL: ${jsonPayload.body.email}\n{port_25_blocked}(must be false): ${done.port_25_blocked}\BUT BAD NEWS IF IT'S FALSE (gmail smtp server have open port 25)`
    )
    assert(Array.isArray(done.dns_records) && (done.dns_records.length > 0),
        `EMAIL: ${jsonPayload.body.email}\n{dns_records}(must be an Array with aleast one element): '${done.valid}'`
    )
    console.log( "SUCCESS", done);
}

/**{[function()]} */
module.exports = [test1, test2, test3, test4]

test1()