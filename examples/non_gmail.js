const lambdaLocal = require('lambda-local');
const assert = require('assert')

async function test1() {
    const jsonPayload = {
        body: { email: "info@cybernetyx.com" }
    }
    const done = (await lambdaLocal.execute({
        event: jsonPayload,
        lambdaPath: './index.js',
        profileName: 'default',
        timeoutMs: 60_000 //one minute
    })).body
    console.log(`EMAIL: ${jsonPayload.body.email} : Testing results ..........................................`);
    assert(done.error === null,
        `EMAIL: ${jsonPayload.body.email}\n{error}(must be null): ${done.error}`
    )
    assert(done.vaild === true,
        `EMAIL: ${jsonPayload.body.email}\n{vaild}(must be true): '${done.vaild}'`
    )
    assert(done.port_25_blocked == true,
        `EMAIL: ${jsonPayload.body.email}\n{port_25_blocked}(must be true): ${done.port_25_blocked}\BUT GREAT NEWS IF IT'S FALSE`
    )
    assert(Array.isArray(done.dns_records) && (done.dns_records.length > 0),
        `EMAIL: ${jsonPayload.body.email}\n{dns_records}(must be an Array with aleast one element): '${done.vaild}'`
    )
    console.log( "SUCCESS", done);
}

async function test2() {
    const jsonPayload = {
        body: { email: "rishav123@outlook.com" }
    }
    const done = (await lambdaLocal.execute({
        event: jsonPayload,
        lambdaPath: './index.js',
        profileName: 'default',
        timeoutMs: 60_000 //one minute
    })).body
    console.log(`EMAIL: ${jsonPayload.body.email} : Testing results ..........................................`);
    assert(done.error === null,
        `EMAIL: ${jsonPayload.body.email}\n{error}(must be null): ${done.error}`
    )
    assert(done.vaild === true,
        `EMAIL: ${jsonPayload.body.email}\n{vaild}(must be true): '${done.vaild}'`
    )
    assert(done.port_25_blocked === true,
        `EMAIL: ${jsonPayload.body.email}\n{port_25_blocked}(must be true): ${done.port_25_blocked}\BUT GREAT NEWS IF IT'S FALSE`
    )
    assert(Array.isArray(done.dns_records) && (done.dns_records.length > 0),
        `EMAIL: ${jsonPayload.body.email}\n{dns_records}(must be an Array with aleast one element): '${done.vaild}'`
    )
    console.log( "SUCCESS", done);
}

//some invalid email hosts

async function test3() {
    const jsonPayload = {
        body: { email: "rishav123@out0000look.com" }
    }
    const done = (await lambdaLocal.execute({
        event: jsonPayload,
        lambdaPath: './index.js',
        profileName: 'default',
        timeoutMs: 60_000 //one minute
    })).body
    console.log(`EMAIL: ${jsonPayload.body.email} : Testing results ..........................................`);
    assert(done.error === "MX record not found",
        `EMAIL: ${jsonPayload.body.email}\n{error}(must be "MX record not found"): ${done.error}`
    )
    assert(done.vaild === false,
        `EMAIL: ${jsonPayload.body.email}\n{vaild}(must be false): '${done.vaild}'`
    )
    assert(done.port_25_blocked == true,
        `EMAIL: ${jsonPayload.body.email}\n{port_25_blocked}(must be true): ${done.port_25_blocked}\BUT GREAT NEWS IF IT'S FALSE`
    )
    assert(Array.isArray(done.dns_records) && (done.dns_records.length === 0),
        `EMAIL: ${jsonPayload.body.email}\n{dns_records}(must be an Empty Array): '${done.vaild}'`
    )
    console.log( "SUCCESS", done);
}

/**{[function()]} */
module.exports = [test1, test2, test3]