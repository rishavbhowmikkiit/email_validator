const {validate} = require('./src/email_valid')

module.exports.handler = async (event) => {
    /**@type {{email:string}} */
    try{
        console.log(event);
        const body = JSON.parse(event.body)
        console.log(event.body)
        const {email} = body
        if(!email) {throw Error("INVALID INPUT: Email is required")}
        return {
            statusCode: 200,
            body: JSON.stringify(await validate(email))
        }
    }catch(e){
        return {
            statusCode: 404,
            body: e.message
        }
    }
}

//unit tests
/*
module.exports.handler(
    {body:{email:'1706340@kiit.ac.in'}}//event object
)
.then((res) => {
    if(res.statusCode === 404){
        console.log('Email is INVALID')
        console.log('Why? Because', res.error, res)
    }else{
        console.log('Email is VALID', res)
    }
})
*/