const { Resolver } = require('dns');
/**
 * @param {string} email - The email address
 * @type {async function(string)}
 * @returns {Promise<{
 *      error:null|string,
 *      valid:boolean,
 *      port_25_blocked:boolean,
 *      domain_valid:boolean,
 *      dns_records:[{ exchange: string, priority: number }]
 *  }>}
 * @description Async function to Validate Email Address
 */
async function validate(email){
    try {
        //validating email pattern
        if(!(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/).test(email)){
            throw new Error('Invalid email address')
        }
        //validating email host
        /** @type {string} */
        const hostname = email.split('@')[1]
        const resolver = new Resolver();
        /**@type {[{ exchange: string, priority: number }]}*/
        const dns_records = await new Promise((resolve, reject) =>{
            try{
                //Retrive Mx records of the email host
                resolver.resolveMx(hostname, (err, address)=>{
                    if(err){reject(err); return;}
                    resolve(address)
                })
            }catch(e){
                reject(e);
            }
        })

        //Identify if email is hosted by gmail
        const google_hosts = dns_records.filter(r=>{
            return /.google.com$/.test(r.exchange)
        })

        if(google_hosts.length){
            //verify gmail accounts
            const {ping_mail_box} = require('./ping_mail_box')
            const {success, port_25_blocked, info} = await ping_mail_box(email)
            return {error:null, valid:(success||port_25_blocked), port_25_blocked, domain_valid:dns_records.length>0, dns_records}  
        }else{
            return {error:null, valid:dns_records.length>0, port_25_blocked:true, domain_valid:dns_records.length>0, dns_records}   
        }
    }catch(e){
        //console.log(e);
        var error = e.message, domain_valid = true, port_25_blocked = true;
        if(e.code === 'ENOTFOUND') {error='MX record not found'; domain_valid=false;}
        return {error, valid:false, port_25_blocked, domain_valid, dns_records:[]}
    }
}

module.exports = {validate}

//unit tests
// A valid email address
/*
validate('1706340@kiit.ac.in')
.then((res) => {
    if(res.error){
        console.log('Email is INVALID')
        console.log('Why? Because', res.error, res)
    }else{
        console.log('Email is VALID', res)
    }
})
*/
// Invalid email address
/*
validate('22706340@gmail.com')
.then((res) => {
    if(res.error){
        console.log('Email is INVALID')
        console.log('Why? Because', res.error, res)
    }else{
        console.log('Email is VALID', res)
    }
})
*/


//Invalid Expression in Email
/*
validate('1706@349@kiit.ac.in')
.then((res) => {
    if(res.error){
        console.log('Email is INVALID')
        console.log('Why? Because', res.error, res)
    }else{
        console.log('Email is VALID', res)
    }
})
*/

//Invalid email host
/*
validate('1706349@aqswkmjnhvyyctc.ac.in')
.then((res) => {
    if(res.error){
        console.log('Email is INVALID')
        console.log('Why? Because', res.error, res)
    }else{
        console.log('Email is VALID', res)
    }
})
*/