const verifier = require('email-verify')
const infoCodes = verifier.verifyCodes;

/**
 * @type {function(email)}
 * @param {string} email - Gmail address to verify
 * @returns {{
        success: boolean,
        port_25_blocked: boolean,
        info: string,
    }}
 */
function ping_mail_box(email) {
    return new Promise((resolve, reject) => {
        try{
            verifier.verify(email, (err, info) => {
                if (err) {
                    resolve({
                        success:true,
                        port_25_blocked: true,
                        info: info.info
                    }); return
                }
                console.log("info.codeinfo.codeinfo.codeinfo.code.....", info.code);
                resolve({
                    success:info.success,
                    port_25_blocked: (info.code === infoCodes.SMTPConnectionTimeout) || (info.code === infoCodes.SMTPConnectionError),
                    info: info.info
                })
            })
        }catch(e){
            reject(e)
        }
    })
}

module.exports = {ping_mail_box}