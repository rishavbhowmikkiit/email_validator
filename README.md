# Email Validator

A pure JS module build to validate Email Address.

**Objective:** Identify invalid Email Addresses to reduce mail bounce on AWS SES & manage subscribtions raised using invalid Email IDs.

### How to verify (Email which are *NOT* hosted by Gmail or Gsuite)

- Check Email syntax

- Check MX records of the email host

- IF the MX records is absent we consider them invalid

- If the MX records is present we consider them valid

- **Output**: `{error:null, vaild:<MX records present>, port_25_blocked:true, domain_valid:<MX records present>, dns_records:[{ exchange: string, priority: number }]}`

### How to verify (Email which are hosted by Gmail or as Gsuite account)

- Check Email syntax

- Check MX records of the email host

- We identify if MX records has host 'google.com'

- Then we connect to 'smtp.gmail.com' on port 25 and ping the mail box

- To Ping the mail box we use: npm module [`email-verify`](https://www.npmjs.com/package/email-verify)

- `email-verify` will return an error if port 25 is blocked or SMTP server causes TIMEOUT, **but we will not treat it as an error, but rather consider it a vaild email where `port_25_blocked` is set to `true` in the response of `email_valid` function call**

- **Output**: `{error:null, vaild:(success||port_25_blocked), port_25_blocked, domain_valid:<MX records present>, dns_records} `

## Deployment Guide

**Folder Structure**

```
+--- 📁 node_modules            <--- Built using 'npm init'
+--- 📁 examples                <--- Examples to test end points
+--- 📁 src
+------ 📄 emai_valid.js        <--- {validate}
+------ 📄 ping_mail_box.js     <--- {ping_mail_box}
+--- 📄 index.js                <--- Statring point for execution of this repo
+--- 📄 package.json            <--- npm package
```

**index.js**

This file must be modified depending on deployment enviroment.

If being deployed on a virtual machine or fully manage instance instance (heroku or azure apps), which requires stand alone server, index.js shall be fitted with a http server.

If being deployed as lambda function, then fitted with accodringly

### AWS lambda function

**Function**

```js
// './index.js'
module.exports.handler = async (event) => {
    /**@type {{email:string}} */
    try{
        const {email} = event.body
        if(!email) {throw Error("INVALID INPUT: Email is required")}
        return {
            statusCode: 200,
            body: await validate(email)
        }
    }catch(e){
        return {
            statusCode: 404,
            body: e.message
        }
    }
}
```

**Event**

```js
{
    body: { 
        email: '<email address to validate>'
    }
}
```

**Result**

```js
{
    body: {
        error: null|string,         //error message during validation
        vaild: boolean,             //if the email can be considered valid
        port_25_blocked: boolean,   //if port 25 blocked
        domain_valid: boolean,      //if domain name has mx record
        dns_records: [              //<mx records>
            { exchange: string, priority: number }
        ]
    }
}
```

**Testing the lambda function in local**
```
npm i
npm test
```

*Note: `npm test` may take some time to complete (upto 3 mins )*